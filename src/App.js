import React from 'react';
import PageTitle from "./components/PageTitle";
import DataFetcher from "./components/DataFetcher";
import styled from "styled-components";

const Container = styled.div`
   max-width: 800px;
   margin: auto;
`;

const App = () => {
    return (
        <Container>
            <PageTitle title={"Markedsinformasjon Bitcoin"}/>
            <DataFetcher/>
        </Container>
    );
};

export default App;