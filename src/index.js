import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';


import { BrowserRouter as Router, Route } from 'react-router-dom';
import App from './App';

/**
 * Oppretter en container (Container) for sideinnholdet.
 * Legger til sidetittel (PageTitle) og komponent for å hente data fra API (DataFetcher)
 */



ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Route path="/:page" component={App}></Route>
    </Router>
    
  </React.StrictMode>,
  document.getElementById('root')
);
