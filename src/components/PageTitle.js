import React from 'react';
import PropTypes from 'prop-types';

/**
 * Returner H1-tekst som brukes som tittel for siden
 * @title: tittel som skal vises
 */

const PageTitle = (props) => {
	return (
		<h1>{props.title}</h1>
	);
};

PageTitle.propTypes = {
	title: PropTypes.string.isRequired
}

export default PageTitle;