import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

/**
 * Knapp som representerer en spesifikk side.
 * @selectedIndex: Om knappen representerer nåværende valgte side
 * @index: Hvilke side knappren representerer.
 * @onClick:  Funksjon for å håndtere klikk.
 */

const Button = styled.button`
	margin-left: 5px;
	margin-right: 5px;
	border-radius: 10px;
	height: 30px;
	width: 30px;
	border: none;
	outline: none;
	cursor:pointer;
	transition: background-color 0.1s;
	
	background-color: ${props => props.selected ? "#283593" : "white"};
	color: ${props => props.selected ? "white" : "black"};
	
	&:hover {
		border: 1.5px solid #283593;
	}
	
	&:active{
		background: #1d2669;
		color: white;
	}

`;

const IndexButton = (props) => {
	return (
		<Button
			onClick={props.onClick}
			selected={props.selectedIndex}>
				{props.index + 1}
		</Button>
	);
};

IndexButton.propTypes = {
	selectedIndex: PropTypes.bool.isRequired,
	index: PropTypes.number.isRequired,
	onClick: PropTypes.func.isRequired
}



export default IndexButton;