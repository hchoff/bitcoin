import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

/**
 * Knapp med spesifisert ikon som brukes til å navigere frem
 * og tilbake blant sidene i paginator.
 * @materialIcon: Material-ikon som vises på knappen
 * @onClick: Funksjon for å håndtere klikk
 * @enabled: Om knappen er deaktivert eller ikke
 */

const Button = styled.button`
	padding: 0;
	width: 35px;
	height: 35px;
	border-radius: 10px;
	border: none;
	outline: none;
	transition: 0.2s;
	cursor: pointer;
	background: none;
	
	&:hover:enabled{
		background-color: #eee;
	}
	
	&:active:enabled{
	background: lightgray;
	}
`;

const NavigationButton = (props) => {
	return (
		<Button onClick={props.onClick} disabled={!props.enabled}>
			<span className="material-icons" style={{lineHeight: "35px"}}>
				{props.materialIcon}
			</span>
		</Button>
	);
};

NavigationButton.propTypes = {
	materialIcon: PropTypes.string.isRequired,
	onClick: PropTypes.func.isRequired,
	enabled: PropTypes.bool.isRequired
}

export default NavigationButton;