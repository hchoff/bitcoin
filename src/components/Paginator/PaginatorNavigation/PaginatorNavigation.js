import React from 'react';
import NavigationButton from "./NavigationButton";
import PropTypes from 'prop-types';
import IndexButton from "./IndexButton";
import styled from 'styled-components';

/**
 * Viser og håndterer knapper for å navigere blant sidene.
 * "frem" og "tilbake"-knapp samt en knapp for hver side.
 * @onPreviousClick: funksjon for å håndtere klikk på tilbake-knappen
 * @onNextClick: funksjon for å håndtere klikk på neste-knappen
 * @onIndexClick: funksjon for håndtere klikk på en sideknapp
 * @currentPage: Nåværende valgte side.
 * @numPages: Totalt antall sider.
 */

const Div = styled.div`
	display: inline-flex;
	align-items: center;
	padding: 7px;
	border-radius: 10px;
	box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
`;

const PaginatorNavigation = (props) => {
	const previousBtnEnabled = props.currentPage !== 0;
	const nextBtnEnabled = props.currentPage !== props.numPages - 1;
	const indexButtons = [];

	const startPage = Math.min(Math.max(0, props.currentPage - 2), (props.numPages - 5));

	if(props.currentPage > 2)
		indexButtons.push(<IndexButton key={0} index={0} onClick={() => props.onIndexClick(0)} selectedIndex={false}/>);

	for(let i = startPage; i < (5 + startPage); i++){
		const selected = props.currentPage === i;
		indexButtons.push(<IndexButton key={i} index={i} onClick={() => props.onIndexClick(i)} selectedIndex={selected}/>);
	}

	if(props.currentPage < props.numPages - 2)
		indexButtons.push(<IndexButton key={props.numPages-1} index={props.numPages-1} onClick={() => props.onIndexClick(props.numPages-1)} selectedIndex={false}/>);

	

	
	
	return (
		<Div>
			<NavigationButton
				enabled={previousBtnEnabled}
				onClick={props.onPreviousClick}
				materialIcon={"keyboard_arrow_left"}/>
			<NavigationButton
				enabled={nextBtnEnabled}
				onClick={props.onNextClick}
				materialIcon={"keyboard_arrow_right"}/>
			{indexButtons}
			
		</Div>
	);
};

PaginatorNavigation.propTypes = {
	onPreviousClick: PropTypes.func.isRequired,
	onNextClick: PropTypes.func.isRequired,
	onIndexClick: PropTypes.func.isRequired,
	currentPage: PropTypes.number.isRequired,
	numPages: PropTypes.number.isRequired
}

export default PaginatorNavigation;