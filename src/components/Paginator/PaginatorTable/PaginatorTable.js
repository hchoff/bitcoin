import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import FormatDate from "./FormatDate";
import FormatCurrency from "./FormatCurrency";
import FormatPercentageChange from "./FormatPercentageChange";

/**
 * Viser et sett med data i en tabell.
 * @selection: det valgte subsettet av data som skal vises
 */

const Table = styled.table`
	margin-top: 20px;
	margin-bottom: 20px;
	width: 100%;
	border-radius: 10px;
	border-spacing: 0;
	box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);

	tbody tr:last-child td{
		border-bottom: none;
	}
	
	thead td:first-child{
		border-top-left-radius: 10px;
	}
	
	thead td:last-child{
		border-top-right-radius: 10px;
	}
	
	thead td{
		padding-top: 20px;
		padding-bottom: 15px;
		font-weight: bold;
		color:white;
		background-color: #283593;
	}
	
	td{
		padding: 10px;
		border-bottom: 1px solid #ddd;
		font-size: 14px;
	}
	
	tbody{
		color: darkslategray;
	}
	
	tbody tr:nth-child(odd){
		background-color: #f8f7fe;
	}
`;

const PaginatorTable = (props) => {
	
	return (
		<Table>
			<thead>
				<tr>
					<td onClick={() => props.onHeaderClick(0)}>Dato</td>
					<td onClick={() => props.onHeaderClick(1)} align={"right"}>Høy</td>
					<td onClick={() => props.onHeaderClick(2)} align={"right"}>Lav</td>
					<td onClick={() => props.onHeaderClick(3)} align={"right"}>Start</td>
					<td onClick={() => props.onHeaderClick(4)} align={"right"}>Slutt</td>
					<td onClick={() => props.onHeaderClick(5)} align={"right"}>Endring</td>
				</tr>
			</thead>
			<tbody>
				{props.selection.map(value => {
					return (
						<tr key={value.time}>
							<td><FormatDate value={value.time}/></td>
							<td align={"right"}><FormatCurrency value={value.high}/></td>
							<td align={"right"}><FormatCurrency value={value.low}/></td>
							<td align={"right"}><FormatCurrency value={value.open}/></td>
							<td align={"right"}><FormatCurrency value={value.close}/></td>
							<td align={"right"}><FormatPercentageChange n1={value.open} n2={value.close}/></td>
						</tr>
					)
				})}
			</tbody>
		</Table>
	);
};

PaginatorTable.propTypes = {
	selection: PropTypes.array.isRequired
}

export default PaginatorTable;