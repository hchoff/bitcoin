import PropTypes from 'prop-types';

/**
 * Formatterer og returnerer et UNIX-timestamp som en datostreng
 * @value: Timestamp som skal formatteres.
 */

const FormatDate = (props) => {
	const date = new Date(props.value * 1000);
	
	return (
		new Intl.DateTimeFormat("no-NB", {
			day: "2-digit",
			month: "2-digit",
			year: "numeric",
			hour12: false
		}).format(date)
	);
};

FormatDate.propTypes = {
	value: PropTypes.number.isRequired
}

export default FormatDate;