import PropTypes from 'prop-types';

/**
 * Formatterer og returnerer et tall som valuta.
 * @value: verdi som skal formatteres.
 */

const FormatCurrency = (props) => {
	return (
		new Intl.NumberFormat("no-NB",{
			style: "currency",
			currency: "USD"
		}).format(props.value)
	);
};

FormatCurrency.propTypes = {
	value: PropTypes.number.isRequired
}

export default FormatCurrency;