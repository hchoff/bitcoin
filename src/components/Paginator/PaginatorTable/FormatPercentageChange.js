import React from 'react';
import PropTypes from 'prop-types';

/**
 * Kalkulerer forskjellen mellom 2 tall (n1, n2) og returnerer
 * differansen formattert som prosent. Grønn tekst hvis det er en økning, og
 * rød tekst hvis det er en nedgang.
 * @n1: Første verdi.
 * @nw: Andre verdi.
 */

const FormatPercentageChange = (props) => {
	const change = (props.n2 - props.n1) / props.n1;
	let color = "green";
	
	if(change < 0)
		color = "red";
	
	return (
		<div style={{color: color}}>
			{new Intl.NumberFormat("no-NB", {
				style: "percent",
				minimumFractionDigits: 2
			}).format(change)}
		</div>
	);
};

FormatPercentageChange.propTypes = {
	n1: PropTypes.number.isRequired,
	n2: PropTypes.number.isRequired
};

export default FormatPercentageChange;