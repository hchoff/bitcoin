import React, {useEffect, useRef, useState} from 'react';
import PropTypes from 'prop-types';
import PaginatorTable from "./PaginatorTable/PaginatorTable";
import PaginatorNavigation from "./PaginatorNavigation/PaginatorNavigation";
import { useParams } from 'react-router-dom';

/**
 * Viser et subset av data fra APIet i tabellen (PaginatorTable) basert på PAGE_SIZE og valg
 * fra bruker gjennom navigasjon (PaginatorNavigation).
 * @data: array med data som skal pagineres
 */

const PAGE_SIZE = 4;

const Paginator = (props) => {
	const [selection, setSelection] = useState([]);
	const numPages = useRef(0);
	const currentPage = useRef(0);
	const { page } = useParams();
	
	useEffect(() => {
		currentPage.current = page - 1;
		props.data.sort((a, b) => (a.time > b.time) ? -1 : 1);
		numPages.current = Math.ceil(props.data.length / PAGE_SIZE);
		setSelection(props.data.slice(0, PAGE_SIZE));
	}, [props.data]);
	
	return (
		<div>
			<PaginatorNavigation
				currentPage={currentPage.current}
				numPages={numPages.current}
				onIndexClick={setPage}
				onNextClick={nextPage}
				onPreviousClick={previousPage}/>
			<PaginatorTable 
				onHeaderClick={(i) => sortTable(i)}
				selection={selection}/>
		</div>
	);
	
	function nextPage(){
		currentPage.current = currentPage.current + 1;
		updateSelection();
	}
	
	function previousPage(){
		currentPage.current = currentPage.current - 1;
		updateSelection();
	}
	
	function setPage(index){
		currentPage.current = index;
		updateSelection();
	}
	
	function updateSelection(){
		setSelection(props.data.slice(currentPage.current * PAGE_SIZE, (currentPage.current + 1) * PAGE_SIZE));
	}

	function sortTable(i){
		console.log(i);
		switch(i){
			case 0:
				props.data.sort((a, b) => (a.time > b.time) ? -1 : 1);
				break;
			case 1:
				props.data.sort((a, b) => (a.high > b.high) ? -1 : 1);
				break;
			case 2:
				props.data.sort((a, b) => (a.low > b.low) ? -1 : 1);
				break;
			case 3:
				props.data.sort((a, b) => (a.open > b.open) ? -1 : 1);
				break;
			case 4:
				props.data.sort((a, b) => (a.close > b.close) ? -1 : 1);
				break;
		}
	
		updateSelection();
	}

	


};

Paginator.propTypes = {
	data: PropTypes.array.isRequired
};

export default Paginator;