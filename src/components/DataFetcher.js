import React, {useEffect, useState} from 'react';
import Paginator from "./Paginator/Paginator";

/**
 * Henter data fra APIet, og viser tabellen (Paginator) når dataen er lastet.
 * Viser tekst ved lasting av data, eller feilmelding hvis feil
 * oppstår ved henting av data.
 */

const URL = "https://min-api.cryptocompare.com/data/v2/histoday?fsym=BTC&tsym=USD&limit=100&api_key=8ae55d463e1bf8d38b4a502ca47512f9b1dec21533ad9af7acb993e8ba952bc2";

const DataFetcher = () => {
	const [data, setData] = useState();
	const [isLoading, setLoading] = useState(true);
	const [isError, setError] = useState(false);
	
	useEffect(() => {
		fetchData();
	}, []);
	
	if(isLoading)
		return <p>Laster data...</p>;
	else if(isError)
		return <p>Feil ved henting av data</p>;
	else
		return <Paginator data={data}/>;
	
	
	function fetchData(){
		fetch(URL)
			.then(response => {
				if(response.ok)
					return response.json();
				else
					throw new Error()
			})
			.then(data => {
				if(data["Response"] !== "Success")
					throw new Error();
				else
					setData(data["Data"]["Data"]);
			})
			.catch(() => setError(true))
			.finally(() => setLoading(false));
	}
};

export default DataFetcher;